--Хисамутдинов Раиль, группа 11-303

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs@(x:other) 
  | snd maxDiv == minimum (map snd xs) = [maxDiv]  | otherwise = xs
    where maxDiv = getMaxDivision x other

getMaxDivision x [] = x
getMaxDivision a1@(a,b) [a2@(c,d)] = if (div a b) > (div c d) then a1 else a2
getMaxDivision x@(a,b) (y:other) = if (div a b) > (div c d) then x else otherMax
  where otherMax@(c,d) = getMaxDivision y other

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters sps = foldl (++) [] $ map singleCargo sps
singleCargo :: Spaceship a -> [Load a]
singleCargo (Cargo loads) = filter isOrbiter loads
    where isOrbiter (Orbiter _) = True
          isOrbiter (Probe _) = False
singleCargo (Rocket h sps) = orbiters sps

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier (x:xs) = (getName x) : (finalFrontier xs)
  where getName (Warp _) = "Kirk"
        getName (BeamUp _) = "Kirk"
        getName (IsDead _) = "McCoy"
        getName (LiveLongAndProsper) = "Spock"
        getName (Fascinating) = "Spock"

