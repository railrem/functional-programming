-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown contact = case contact of
On -> False 
Off -> False
False -> True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const x) = x
eval (Mult x y) = eval x * eval y
eval (Add x y) = eval x + eval y
eval (Sub x y) = eval x - eval y

--term1 = Const 2 
--term2 = Const 3
--term3 = Mult term1 term2
--term4 = Add term1 term2
--term5 = Sub term2 term1

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Mult (Add (Term x) (Term z)) (Term y)) = Add (Mult (simplify x) (simplify y)) (Mult (simplify z)(simplify y))
simplify (Mult (Term z) (Add (Term x) (Term y))) = Add (Mult (simplify z) (simplify x)) (Mult (simplify z)(simplify y))
simplify (Mult (Term z) (Sub (Term x) (Term y))) = Sub (Mult (simplify z) (simplify x)) (Mult (simplify z)(simplify y))
simplify (Mult (Sub (Term x) (Term y)) (Term z)) = Sub (Mult (simplify x) (simplify z)) (Mult (simplify y)(simplify z))
simplify (Term t) = t