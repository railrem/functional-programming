-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests =   [testGroup "HWs"
               [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ],
           testGroup "eval1" [
                       testCase "eval1 (\\x . x) y == y"
                            $ show (eval1 (App (Lam (Var 'x') (Var 'x')) (Var 'y'))) @?= "y",
                       testCase "eval1 (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                            $ show (eval1 (App (Lam (Var 'x')
                                                       (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                                               (Lam (Var 'z') (Var 'z'))))
                                        @?= "(\\y . (\\z . z) y)",
                       testCase "eval1 (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\y . (\\z . z) y) (\\u . \\v . v)"
                            $ show (eval1 (App (App (Lam (Var 'x')
                                                       (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                                               (Lam (Var 'z') (Var 'z')))
                                            (Lam (Var 'u') (Lam (Var 'v') (Var 'v')))))
                                        @?= "(\\y . (\\z . z) y) (\\u . (\\v . v))",
                      testCase "eval1 (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                           $ show (eval1 (Lam (Var 'x') (App (Lam (Var 'x') (Var 'x')) (Var 'x'))))
                                   @?= "(\\x . (\\x . x) x)",
                      testCase "eval1 (\\x . \\y . x) y = (\\y . a)"
                           $ show (eval1 (App (Lam (Var 'x') (Lam (Var 'y') (Var 'x'))) (Var 'y')))
                                  @?= "(\\y . a)"
                ],
           testGroup "eval_many" [
                       testCase "eval_many (\\x . x) y == y"
                            $ show (eval_many (App (Lam (Var 'x') (Var 'x')) (Var 'y'))) @?= "y",
                       testCase "eval_many (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                            $ show (eval_many (App (Lam (Var 'x')
                                                       (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                                               (Lam (Var 'z') (Var 'z'))))
                                        @?= "(\\y . (\\z . z) y)",
                       testCase "eval_many (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\u . \\v . v)"
                            $ show (eval_many (App (App (Lam (Var 'x')
                                                       (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                                              (Lam (Var 'z') (Var 'z')))
                                            (Lam (Var 'u') (Lam (Var 'v') (Var 'v')))))
                                        @?= "(\\u . (\\v . v))",
                       testCase "eval_many (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                            $ show (eval_many (Lam (Var 'x') (App (Lam (Var 'x') (Var 'x')) (Var 'x'))))
                                    @?= "(\\x . (\\x . x) x)",
                       testCase "eval_many (\\x . \\y . x) y = (\\y . a)"
                            $ show (eval_many (App (Lam (Var 'x') (Lam (Var 'y') (Var 'x'))) (Var 'y')))
                                    @?= "(\\y . a)"

                 ]
            ]


